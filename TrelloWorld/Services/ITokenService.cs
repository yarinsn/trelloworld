﻿using Manatee.Trello;
using System.Web;
using TrelloWorld.Models;

namespace TrelloWorld.Services
{
    public interface ITokenService
    {
        void PersistToken(string tokenValue);

        void RemoveTokenIdCookie();

        string GetUserTokenValue();

        bool IsCookieExists();
    }
}
