﻿using Manatee.Trello;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrelloWorld.Services
{
    public interface ITrelloService
    {
        IEnumerable<Board> GetMemberBoards();

        string GetBoardName(string boardId);

        IEnumerable<Card> GetCards(string boardId);

        Card GetCard(string cardId);

        Card UpdateComment(string cardId, string newComment);
    }
}
