﻿using System;
using System.Text;
using System.Web;
using TrelloWorld.Common;
using TrelloWorld.Models;

namespace TrelloWorld.Services
{
    public class TokenService : ITokenService
    {
        private TokenObjectDBContext db = new TokenObjectDBContext();
        private ITokenIdGenerator _tokenIdGenerator;

        public TokenService(ITokenIdGenerator tokenIdGenerator)
        {
            _tokenIdGenerator = tokenIdGenerator;
        }

        public void PersistToken(string tokenValue)
        {
            var persistedToken = SaveTokenToDB(tokenValue);

            AddTokenIdToCookie(persistedToken.ID); 
        }

        public void RemoveTokenIdCookie()
        {
            HttpCookie currentUserCookie = HttpContext.Current.Request.Cookies[TrelloWorldConfiguration.CookieKey];

            if (currentUserCookie == null)
                return;

            HttpContext.Current.Response.Cookies.Remove(TrelloWorldConfiguration.CookieKey);
            currentUserCookie.Expires = DateTime.Now.AddDays(-10);
            currentUserCookie.Value = null;
            HttpContext.Current.Response.SetCookie(currentUserCookie);
        }

        public string GetUserTokenValue()
        {
            var tokenId = GetTokenId();

            try
            {
                var token = db.TokenObjects.Find(tokenId);
                return token.Value;
            }
            catch (Exception)
            {
                RemoveTokenIdCookie();
                throw;
            }
        }

        public bool IsCookieExists()
        {
            var cookie = GetTokenIdCookie();

            return cookie != null;
        }

        private TokenObject SaveTokenToDB(string tokenValue)
        {
            try
            {
                var tokenId = _tokenIdGenerator.Generate(tokenValue);

                var persistedToken = db.TokenObjects.Add(new TokenObject() { Value = tokenValue, ID = tokenId });
                db.SaveChanges();

                return persistedToken;
            }
            catch (Exception)
            {
                RemoveTokenIdCookie();
                throw;
            }
        }

        private void AddTokenIdToCookie(string tokenId)
        {
            var tokenIdCookie = new HttpCookie(TrelloWorldConfiguration.CookieKey, tokenId);
            tokenIdCookie.Expires = DateTime.Now.AddDays(1);

            HttpContext.Current.Response.Cookies.Add(tokenIdCookie);
        }

        private string GetTokenId()
        {
            var tokenIdCookie = GetTokenIdCookie();

            if (tokenIdCookie == null)
                throw new UnauthorizedAccessException("User is not authorized");

            return tokenIdCookie.Value; //todo: carful. change when id will be string
        }

        private HttpCookie GetTokenIdCookie()
        {
            return HttpContext.Current.Request.Cookies[TrelloWorldConfiguration.CookieKey];
        }
    }
}