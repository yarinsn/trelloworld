﻿using Manatee.Trello;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrelloWorld.Services
{
    public interface IAuthService
    {
        bool IsUserAuthenticated();

        string GenerateAuthRouthUrl();

        bool AuthenticateUser(string tokenValue);

        void LogoutUser();

        TrelloAuthorization GetUserAuthObject();
    }
}
