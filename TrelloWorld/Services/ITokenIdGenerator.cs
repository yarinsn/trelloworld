﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrelloWorld.Services
{
    public interface ITokenIdGenerator
    {
        string Generate(string tokenValue);
    }
}
