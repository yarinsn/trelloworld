﻿using Manatee.Trello;
using System;
using TrelloWorld.Common;

namespace TrelloWorld.Services
{
    public class AuthService : IAuthService
    {
        private ITokenService _tokenService;

        public AuthService(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        public bool IsUserAuthenticated()
        {
            return _tokenService.IsCookieExists();
        }

        public string GenerateAuthRouthUrl()
        {
            var currentDomainUrl = string.Format("http://{0}/{1}", TrelloWorldConfiguration.CurrentDomain, "Auth/Login");

            var url = string.Format("https://trello.com/1/authorize?callback_method=fragment&return_url={0}&scope=read%2Cwrite%2Caccount&expiration=1day&name=yarintestapp&key={1}", Uri.EscapeDataString(currentDomainUrl), TrelloWorldConfiguration.AppKey);

            return url;
        }

        public bool AuthenticateUser(string tokenValue)
        {
            if (string.IsNullOrEmpty(tokenValue))
                return false;

            _tokenService.PersistToken(tokenValue);

            return true;
        }

        public void LogoutUser()
        {
            _tokenService.RemoveTokenIdCookie();
        }

        public TrelloAuthorization GetUserAuthObject()
        {
            var tokenValue = _tokenService.GetUserTokenValue();

            var auth = new TrelloAuthorization
            {
                AppKey = TrelloWorldConfiguration.AppKey,
                UserToken = tokenValue
            };

            return auth;
        }
    }
}