﻿using Manatee.Trello;
using Manatee.Trello.ManateeJson;
using Manatee.Trello.WebApi;
using System.Collections.Generic;
using TrelloWorld.Common;

namespace TrelloWorld.Services
{
    public class TrelloService : ITrelloService
    {
        private IAuthService _authService;

        public TrelloService(IAuthService authService)
        {
            _authService = authService;

            Init();
        }

        public IEnumerable<Board> GetMemberBoards()
        {
            var member = GetMember();

            return (IEnumerable<Board>)member.Boards;
        }

        public string GetBoardName(string boardId)
        {
            var board = GetBoard(boardId);

            return board.Name;
        }

        public IEnumerable<Card> GetCards(string boardId)
        {
            var board = GetBoard(boardId);

            return (IEnumerable<Card>)board.Cards;
        }

        public Card GetCard(string cardId)
        {
            var auth = _authService.GetUserAuthObject();

            var card = new Card(cardId, auth);

            return card;
        }

        public Card UpdateComment(string cardId, string newComment)
        {
            var card = GetCard(cardId);

            card.Comments.Add(newComment);

            return card;
        }

        private Member GetMember()
        {
            var auth = _authService.GetUserAuthObject();

            var token = new Token(auth.UserToken, auth);

            return token.Member;
        }

        private Board GetBoard(string boardId)
        {
            var auth = _authService.GetUserAuthObject();

            var board = new Board(boardId, auth);

            return board;
        }

        private void Init()
        {
            var serializer = new ManateeSerializer();
            TrelloConfiguration.Serializer = serializer;
            TrelloConfiguration.Deserializer = serializer;
            TrelloConfiguration.JsonFactory = new ManateeFactory();
            TrelloConfiguration.RestClientProvider = new WebApiClientProvider();
            TrelloAuthorization.Default.AppKey = TrelloWorldConfiguration.AppKey;
        }
    }
}