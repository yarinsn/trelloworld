﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace TrelloWorld.Services
{
    public class TokenIdGenerator : ITokenIdGenerator
    {
        private Random randomizer = new Random();
        private Regex regex = new Regex("^[a-zA-Z0-9]*$");

        public string Generate(string tokenValue)
        {
            var sb = new StringBuilder();

            var tokenValueLength = tokenValue.Length;
            var idSize = 6;

            for (int i = 0; i < idSize; i++)
            {
                var charPosition = randomizer.Next(tokenValueLength);
                var idChar = tokenValue[charPosition];

                if (Char.IsLetter(idChar) || Char.IsNumber(idChar))
                    sb.Append(idChar);
                else
                    sb.Append('a');
            }

            var tokenId = sb.ToString();

            return tokenId;
        }
    }
}