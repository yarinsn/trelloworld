﻿window.onload = function () {
    function extractToken() {

        var token = location.hash.substr(7);

        window.location.replace('/auth/fetchtoken?tokenValue=' + token);
    }

    extractToken();
}