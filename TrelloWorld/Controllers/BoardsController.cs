﻿using System;
using System.Web.Mvc;
using TrelloWorld.Services;

namespace TrelloWorld.Controllers
{
    public class BoardsController : Controller
    {
        private ITrelloService _trelloService;

        public BoardsController(ITrelloService trelloService)
        {
            _trelloService = trelloService;
        }

        // GET: Boards
        public ActionResult Index()
        {
            try
            {
                var userBoards = _trelloService.GetMemberBoards();
                return View(userBoards);
            }
            catch (UnauthorizedAccessException)
            {
                return RedirectToAuthErrorPage();
            }
            catch (NullReferenceException)
            {
                return RedirectToAuthErrorPage();
            }
            catch (ArgumentException)
            {
                return RedirectToAppErrorPage();
            }
        }

        public ActionResult Cards(string boardId) 
        {
            try
            {
                var cards = _trelloService.GetCards(boardId);
                ViewBag.BoardName = _trelloService.GetBoardName(boardId);

                return View(cards);
            }
            catch (NullReferenceException)
            {
                return RedirectToAuthErrorPage();
            }
            catch (UnauthorizedAccessException)
            {
                return RedirectToAuthErrorPage();
            }
        }

        public ActionResult AddComment(string cardId)
        {
            try
            {
                var card = _trelloService.GetCard(cardId);

                ViewBag.CardName = card.Name;

                return View(card);
            }
            catch (NullReferenceException)
            {
                return RedirectToAuthErrorPage();
            }
            catch (UnauthorizedAccessException)
            {
                return RedirectToAuthErrorPage();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddComment(string cardId, string newComment)
        {
            try
            {
                var card = _trelloService.UpdateComment(cardId, newComment);

                return RedirectToAction("Cards", "Boards", new { boardId = card.Board.Id });
            }
            catch (NullReferenceException)
            {
                return RedirectToAuthErrorPage();
            }
            catch (UnauthorizedAccessException)
            {
                return RedirectToAuthErrorPage();
            }
        }

        private ActionResult RedirectToAuthErrorPage()
        {
            return View("AuthError");
        }

        private ActionResult RedirectToAppErrorPage()
        {
            return View("AppError");
        }
    }
}