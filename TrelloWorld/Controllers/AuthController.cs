﻿using System;
using System.Web.Mvc;
using TrelloWorld.Services;

namespace TrelloWorld.Controllers
{
    public class AuthController : Controller
    {
        private IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        // GET: Auth
        public ActionResult Index()
        {
            if (!_authService.IsUserAuthenticated()) 
            {
                var authRouteUrl = _authService.GenerateAuthRouthUrl();

                return Redirect(authRouteUrl);
            }
            
            return RedirectToUserBoards();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult FetchToken(string tokenValue)
        {
            try
            {
                if (_authService.AuthenticateUser(tokenValue))
                    return RedirectToUserBoards();
            }
            catch (Exception)
            {
                _authService.LogoutUser();
                return View("AppError");
            }

            return RedirectHome(); //user denied access
        }

        public ActionResult Logout()
        {
            _authService.LogoutUser();

            return RedirectHome();
        }

        private ActionResult RedirectToUserBoards()
        {
            return RedirectToAction("Index", "Boards");
        }

        private ActionResult RedirectHome()
        {
            return RedirectToAction("Index", "Home");
        }
    }
}