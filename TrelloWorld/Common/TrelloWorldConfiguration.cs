﻿using System.Web;

namespace TrelloWorld.Common
{
    public static class TrelloWorldConfiguration
    {
        public static readonly string CookieKey = "AAwkf993adf_7";

        public static readonly string AppKey = "79749e76d8b3af6e74bfa91b2606a340";

        public static readonly string CurrentDomain;

        public static readonly bool IsLocal;

        static TrelloWorldConfiguration()
        {
            CurrentDomain = GetCurrentDomain();

            IsLocal = CheckIfLocal(CurrentDomain);
        }

        private static bool CheckIfLocal(string CurrentDomain)
        {
            return CurrentDomain.ToLower().IndexOf("localhost") >= 0;
        }

        private static string GetCurrentDomain()
        {
            return HttpContext.Current.Request.Url.Authority;
        }
    }
}