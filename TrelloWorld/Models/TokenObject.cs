﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TrelloWorld.Models
{
    public class TokenObject
    {
        public string ID { get; set; }
        public string Value { get; set; }
    }

    public class TokenObjectDBContext : DbContext
    {
        public DbSet<TokenObject> TokenObjects { get; set; }
    }
}